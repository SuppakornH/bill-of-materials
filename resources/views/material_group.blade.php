@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">ตั้งค่า : กลุ่มวัสดุ/รายการวัสดุ</h4>
            @if (session('status'))
                {!! session('status') !!}
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20"></h4>
                        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 10%;">ลำดับ</th>
                                    <th style="text-align: center; width: 40%;">ประเภทวัสดุ</th>
                                    <th style="text-align: center; width: 35%;">กลุ่มวัสดุ</th>
                                    <th style="text-align: center; width: 15%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                @foreach($Material_groups as $Material_group)
                                <tr>
                                    <td style="text-align: center;">{{ ++$i }}</td>
                                    <td style="text-align: center;">{{ $Material_group->type_name }}</td>
                                    <td style="text-align: center;">{{ $Material_group->material_group_id }}</td>
                                    <td style="text-align: center;">
                                        <i class="c-orange-500 ti-pencil-alt" onclick="edit_material_group({{ $Material_group->id }})" style="font-size: 20px; cursor: pointer;" title="แก้ไข"></i>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCreateGroup" tabindex="-1" role="dialog" aria-labelledby="modalCreateGroupLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/insert_material_group" class="form-insert" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCreateGroupLabel">เพิ่มกลุ่มวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-form" role="alert">กรุณากรอกข้อมูลให้ครบถ้วน</div>
                        <div class="alert alert-danger error-code" role="alert">รหัสกลุ่มวัสดุนี้มีอยู่ในระบบแล้ว</div>
                        <div class="form-group">
                            <select id="material_type_id" name="material_type_id" class="form-control">
                                <option hidden="true">...เลือกประเภทวัสดุ...</option>
                                @foreach($Material_types as $Material_type)
                                    <option value="{{ $Material_type->id }}">{{ $Material_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="material_group_id" id="material_group_id" placeholder="รหัสกลุ่มวัสดุ">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-insert">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEditGroup" tabindex="-1" role="dialog" aria-labelledby="modalEditGroupLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/update_material_group" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalEditGroupLabel">แก้ไขข้อมูลกลุ่มวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select id="edit_material_type_id" name="edit_material_type_id" class="form-control">
                                <option hidden="true">...เลือกประเภทวัสดุ...</option>
                                @foreach($Material_types as $Material_type)
                                    <option value="{{ $Material_type->id }}">{{ $Material_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="edit_material_group_id" id="edit_material_group_id" placeholder="รหัสกลุ่มวัสดุ" readonly="true">
                        </div>
                        <input type="hidden" name="edit_id" id="edit_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCreateGroupExcel" tabindex="-1" role="dialog" aria-labelledby="modalCreateGroupExcelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/excel_upload_group" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCreateGroupExcelLabel">เพิ่มกลุ่มวัสดุด้วย Excel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-excel" role="alert">กรุณาเลือกประเภทวัสดุ และ ไฟล์ Excel ที่ต้องการนำข้อมูลเข้า</div>
                        <div class="alert alert-danger error-type" role="alert">นำเข้าได้เฉพาะไฟล์ : .xlsx, .csv</div>
                        <div class="form-group">
                            <select id="excel_material_type_id" name="excel_material_type_id" class="form-control">
                                <option hidden="true">...เลือกประเภทวัสดุ...</option>
                                @foreach($Material_types as $Material_type)
                                    <option value="{{ $Material_type->id }}">{{ $Material_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01"><i class="ti-upload"></i></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="excel_file" id="excel_file" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกไฟล์ Excel : .xlsx, .csv</label>
                            </div>
                        </div>
                        <div style="margin-top: 15px">
                            <a href="/download_excel_group" style="cursor: pointer;">
                                <img src="{{ asset('assets/icon/excel_icon.png') }}" style="width: 30px;">
                                <label style="cursor: pointer;">โหลดแบบฟอร์มไฟล์ Excel</label>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-upload">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var create_btn = "";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-danger' data-toggle='modal' data-target='#modalCreateGroup'>เพิ่มกลุ่มวัสดุ</button>";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-success' data-toggle='modal' data-target='#modalCreateGroupExcel'>เพิ่มกลุ่มวัสดุด้วย Excel</button>";

            $("#dataTable_length").append(create_btn);

            $(".error-form").hide();
            $(".error-code").hide();
            $(".error-excel").hide();
            $(".error-type").hide();
        });

        $(document).on("change", ".custom-file :file", function() {
            var filename = $('input[type=file]').val().split('\\').pop();
            var arr = filename.split('.');
            if(arr[1] == 'csv' || arr[1] == 'xlsx'){
                $(".error-excel").hide();
                $(".error-type").hide();
                $("label.custom-file-label").html(filename);
            }else{
                $(".error-excel").hide();
                $(".error-type").show();
                $('input[type=file]').val('');
                $("label.custom-file-label").html('เลือกไฟล์ Excel : .xlsx, .csv');
            }
        });

        $(document).on("click", ".validate-upload", function() {
            var filename = $('#excel_file').val().split('\\').pop();
            var type = $("#excel_material_type_id").val();
            if(filename == '' || type == '...เลือกประเภทวัสดุ...'){
                $(".error-excel").show();
                $(".error-type").hide();
                return false;
            }else{
                return true;
            }
        });

        $(document).on("click", ".validate-insert", function() {
            var id = $("#material_group_id").val();
            var type = $("#material_type_id").val();
            if(id == '' || type == '...เลือกประเภทวัสดุ...'){
                $(".error-form").show();
                return false;
            }else{
                $.ajax({
                    url: '/setting/check_duplicate',
                    type: 'GET',
                    data: {id: id},
                    success:function(data) {
                        if(data == true){
                            $(".form-insert").submit();
                        }else{
                            $(".error-form").hide();
                            $(".error-code").show();
                        }
                    }
                });
            }
            return false;
        });

        function edit_material_group(id){
            $('#modalEditGroup').modal('show');
            $.ajax({
                url: '/setting/select_material_group',
                type: 'GET',
                data: {id: id},
                success:function(data) {
                    $("#edit_material_type_id option[value='"+data.material_type_id+"']").attr('selected',true);
                    $("#edit_material_group_id").val(data.material_group_id);
                    $("#edit_id").val(data.id);
                }
            });
        }
    </script>
@endsection
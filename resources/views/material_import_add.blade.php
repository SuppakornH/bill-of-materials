@extends('layouts.app')

@section('css')

    <style type="text/css">
        .label_right {
            text-align: right;
        }
    </style>

@endsection

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">นำเข้าวัสดุ</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">เพิ่มรายการนำเข้าวัสดุ</h4>
                        <form action="/material_import/add/insert" method="POST">
                            @csrf
                            <div class="alert alert-danger error-form" role="alert">กรุณากรอกข้อมูลให้ครบถ้วน</div>
                            <div class="row">
                                <div class="col-md-2 label_right">
                                    <label>ปีบัญชี <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="fiscal_year" id="fiscal_year" value="{!! $fiscal_year !!}" placeholder="ปีบัญชี">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>วันที่นำเข้า <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control start-date" value="{!! $date !!}" name="date_import" id="date_import" placeholder="วันที่นำเข้า">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>เลขที่เอกสาร <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="documnet_number" id="documnet_number" placeholder="เลขที่เอกสาร">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>รับจาก <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="import_from" id="import_from" placeholder="รับพัสดุจาก">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>หมายเหตุ : </label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="remark" id="remark" placeholder="หมายเหตุ">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    
                                </div>
                                <div class="col-md-10" style="margin-bottom: 18px;">
                                    <a href="#" class="btn btn-success add_form">เพิ่ม</a>
                                    <a href="#" class="btn btn-danger del_form">ลบ</a>
                                    <input type="hidden" class="count_form" value="1">
                                </div>
                            </div>
                            <div class="row form_1">
                                <div class="col-md-2 label_right">
                                    <label>รายการวัสดุ #<span class="number_of_form">1</span> <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input list="materials" name="material[]" id="material" class="form-control material" placeholder="เลือกรายการวัสดุ" autocomplete="off">
                                        <datalist id="materials">
                                            @foreach($Material_lists as $Material_list)
                                            <option value="{{ $Material_list->id.'-'.$Material_list->material_group_id.'-'.$Material_list->name.' ('.$Material_list->unit.')' }}">
                                            @endforeach
                                        </datalist>
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>จำนวนวัสดุ #<span class="number_of_form">1</span> <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="number" class="form-control amount" name="amount[]" id="amount" placeholder="จำนวนวัสดุ">
                                    </div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>ราคาต่อหน่วย #<span class="number_of_form">1</span> <span style="color : #f44336;">*</span> : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="number" class="form-control price" min="0.01" step="0.01" name="price[]" id="price" placeholder="ราคาต่อหน่วย">
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button class="btn btn-danger validate-insert">บันทึก</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')

    <script type="text/javascript">
        $( document ).ready(function() {
            $('.error-form').hide();
        });

        $(".add_form").on("click",function() {
            var form = $(".form_1").html();
            var count = parseInt($(".count_form").val());
            var count_update = count+1;

            $(".form_"+count).after( "<div class='row form_"+count_update+"'>"+form+"</div>" );

            var new_form = $(".form_"+count_update).children().filter('.label_right').children().children('.number_of_form');
            
            new_form.empty();
            new_form.append(count_update);
            
            $(".count_form").val(count_update);
        });

        $(".del_form").on("click",function() {
            var count = parseInt($(".count_form").val());
            if(count == 1){
                return false;
            }else{
                var count_update = count-1;
                $( ".form_"+count ).remove();
                $(".count_form").val(count_update);
            }            
        });

        $(document).on("focus", ".material", function() {
            $(this).val('');
            $(this).closest('.row').children('div.col-md-4').find('.amount').val('');
            $(this).closest('.row').children('div.col-md-4').find('.price').val('');
        });

        $(document).on("click", ".validate-insert", function() {
            var fiscal_year = $('#fiscal_year').val();
            var date_import = $('#date_import').val();
            var documnet_number = $('#documnet_number').val();
            var import_from = $('#import_from').val();
            var count_form = $('.count_form').val();

            if(fiscal_year == '' || date_import == '' || documnet_number == '' || import_from == '' ){
                $('.error-form').show();
                return false;
            }else{
                for(var i = 1; i <= count_form; i++){
                    var material = $('.form_'+i).find('#material').val();
                    var price = $('.form_'+i).find('#price').val();
                    var amount = $('.form_'+i).find('#amount').val();
                    if(material == '' || price == '' || amount == ''){
                        $('.error-form').show();
                        return false;
                        break;
                    }else{
                        if(i == count_form){
                            return true;
                        }
                    }
                }

            }
            return false;
        });
    </script>

@endsection
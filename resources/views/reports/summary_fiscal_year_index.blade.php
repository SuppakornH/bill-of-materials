@extends('layouts.app')

@section('css')

    <style type="text/css">
        .label_right {
            text-align: right;
        }

        .label_left {
            text-align: left;
        }

        .label_center {
            text-align: center;
        }
    </style>

@endsection

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">รายงาน : สรุปยอดวัสดุคงเหลือ</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h5 class="c-grey-900 mB-20 label_center">ทะเบียนวัสดุ</h5>
                        <h5 class="c-grey-900 mB-20 label_center">{{ $department->name }}</h5>
                        <h5 class="c-grey-900 mB-20 label_center">วัสดุคงเหลือ ณ ปีบัญชี {{ $fiscal_year }}</h5>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 8%">ลำดับ</th>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center;">รายการวัสดุ</th>
                                    <th colspan="2" style="text-align: center; width: 18%">ยอดยกมาปีบัญชี {{ $fiscal_year-1 }}</th>
                                    <th colspan="2" style="text-align: center; width: 18%">ซื้อระหว่างปี</th>
                                    <th colspan="2" style="text-align: center; width: 18%">เบิกใช้ไป</th>
                                    <th colspan="2" style="text-align: center; width: 18%">คงเหลือ</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">จำนวน</th>
                                    <th style="text-align: center;">ราคารวม</th>
                                    <th style="text-align: center;">จำนวน</th>
                                    <th style="text-align: center;">ราคารวม</th>
                                    <th style="text-align: center;">จำนวน</th>
                                    <th style="text-align: center;">ราคารวม</th>
                                    <th style="text-align: center;">จำนวน</th>
                                    <th style="text-align: center;">ราคารวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i = 1;
                                    $sum_total_amount = 0;
                                    $sum_total_price = 0;

                                    $sum_total_amount_old_year_summary = 0;
                                    $sum_total_price_old_year_summary = 0;
                                    $sum_total_amount_import = 0;
                                    $sum_total_price_import = 0;
                                    $sum_total_amount_export = 0;
                                    $sum_total_price_export = 0;
                                    $sum_total_amount_new_year_summary = 0;
                                    $sum_total_price_new_year_summary = 0;
                                ?>
                                @foreach($year_summarys as $year_summary)
                                    <?php 
                                        $sum_total_amount = ($year_summary->years_summary_total_amount + $year_summary->material_imports_detail_total_amount) - $year_summary->material_exports_detail_total_amount;
                                        $sum_total_price = ($year_summary->years_summary_total_price + $year_summary->material_imports_detail_total_price) - $year_summary->material_exports_detail_total_price;

                                        $sum_total_amount_old_year_summary += $year_summary->years_summary_total_amount;
                                        $sum_total_price_old_year_summary += $year_summary->years_summary_total_price;
                                        $sum_total_amount_import += $year_summary->material_imports_detail_total_amount;
                                        $sum_total_price_import += $year_summary->material_imports_detail_total_price;
                                        $sum_total_amount_export += $year_summary->material_exports_detail_total_amount;
                                        $sum_total_price_export += $year_summary->material_exports_detail_total_price;
                                        $sum_total_amount_new_year_summary += $sum_total_amount;
                                        $sum_total_price_new_year_summary += $sum_total_price;
                                    ?>
                                    <tr style="cursor:pointer" onclick="location.href='/report/{{ $year_summary->id }}/{{ $fiscal_year }}/summary'">
                                        <td style="text-align: center;">{{ $i++ }}</td>
                                        <td>ยอดรวม{{ $year_summary->name }}</td>
                                        <td style="text-align: center;">
                                            @if($year_summary->years_summary_total_amount == null)
                                                0
                                            @else
                                                {{ number_format($year_summary->years_summary_total_amount) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($year_summary->years_summary_total_price == null)
                                                0.00
                                            @else
                                                {{ number_format($year_summary->years_summary_total_price, 2) }}
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($year_summary->material_imports_detail_total_amount == null)
                                                0
                                            @else
                                                {{ number_format($year_summary->material_imports_detail_total_amount) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($year_summary->material_imports_detail_total_price == null)
                                                0.00
                                            @else
                                                {{ number_format($year_summary->material_imports_detail_total_price, 2) }}
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($year_summary->material_exports_detail_total_amount == null)
                                                0
                                            @else
                                                {{ number_format($year_summary->material_exports_detail_total_amount) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($year_summary->material_exports_detail_total_price == null)
                                                0.00
                                            @else
                                                {{ number_format($year_summary->material_exports_detail_total_price, 2) }}
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($sum_total_amount == null)
                                                0
                                            @else
                                                {{ number_format($sum_total_amount) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($sum_total_price == null)
                                                0.00
                                            @else
                                                {{ number_format($sum_total_price, 2) }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align: center;">ยอดรวมทั้งหมด</th>
                                    <th style="text-align: center;">
                                        {{ number_format($sum_total_amount_old_year_summary) }}
                                    </th>
                                    <th style="text-align: right;">
                                        {{ number_format($sum_total_price_old_year_summary, 2) }}
                                    </th>
                                    <th style="text-align: center;">
                                        {{ number_format($sum_total_amount_import) }}
                                    </th>
                                    <th style="text-align: right;">
                                        {{ number_format($sum_total_price_import, 2) }}
                                    </th>
                                    <th style="text-align: center;">
                                        {{ number_format($sum_total_amount_export) }}
                                    </th>
                                    <th style="text-align: right;">
                                        {{ number_format($sum_total_price_export, 2) }}
                                    </th>
                                    <th style="text-align: center;">
                                        {{ number_format($sum_total_amount_new_year_summary) }}
                                    </th>
                                    <th style="text-align: right;">
                                        {{ number_format($sum_total_price_new_year_summary, 2) }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            
        });
    </script>
@endsection
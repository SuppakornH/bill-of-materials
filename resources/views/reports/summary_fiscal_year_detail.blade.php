@extends('layouts.app')

@section('css')

    <style type="text/css">
        .label_right {
            text-align: right;
        }

        .label_left {
            text-align: left;
        }

        .label_center {
            text-align: center;
        }
    </style>

@endsection

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">รายงาน : สรุปยอดวัสดุคงเหลือ</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h5 class="c-grey-900 mB-20 label_center">{{ $material_type->name }}คงเหลือ ณ ปีบัญชี {{ $fiscal_year }}</h5>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 8%">ลำดับ</th>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 12%">กลุ่มวัสดุ</th>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 12%">รหัส GPSC</th>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 33%">รายการวัสดุ</th>
                                    <th colspan="3" style="text-align: center; width: 25%">คงเหลือ</th>
                                    <th rowspan="2" style="padding-bottom: 35px; text-align: center; width: 10%">หมายเหตุ</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">จำนวน</th>
                                    <th style="text-align: center;">ราคารวม</th>
                                    <th style="text-align: center;">จำนวน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i = 1;
                                    $group_id = $year_summary_lists[0]->material_group_id;
                                    $group_total_amount = 0;
                                    $group_total_price = 0;
                                    $total_amount_all = 0;
                                    $total_price_all = 0;
                                ?>
                                <tr>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center;"></td>
                                    <td><b>{{ $material_type->name }}</b></td>
                                    <td style="text-align: center;">
                                    </td>
                                    <td style="text-align: right;">
                                    </td>
                                    <td style="text-align: right;">
                                    </td>
                                    <td style="text-align: center;">
                                    </td>
                                </tr>
                                @foreach($year_summary_lists as $year_summary_list)
                                    <?php 
                                        if($group_id != $year_summary_list->material_group_id){
                                    ?>
                                            <tr>
                                                <td style="text-align: center;"><b>รวม</b></td>
                                                <td style="text-align: center;"><b>{{ $group_id }}</b></td>
                                                <td style="text-align: center;"></td>
                                                <td></td>
                                                <td style="text-align: center;">
                                                    <b>{{ number_format($group_total_amount) }}</b>
                                                </td>
                                                <td style="text-align: right;">
                                                </td>
                                                <td style="text-align: right;">
                                                    <b>{{ number_format($group_total_price, 2) }}</b>
                                                </td>
                                                <td style="text-align: center;">
                                                </td>
                                            </tr>
                                    <?php
                                            $i = 1;
                                            $group_total_amount = 0;
                                            $group_total_price = 0;
                                        }
                                        $group_id = $year_summary_list->material_group_id;
                                        $group_total_amount += (int)$year_summary_list->amount;
                                        $group_total_price += $year_summary_list->total_price;
                                        $total_amount_all += (int)$year_summary_list->amount;
                                        $total_price_all += $year_summary_list->total_price;
                                    ?>
                                    <tr>
                                        <td style="text-align: center;">{{ $i++ }}</td>
                                        <td style="text-align: center;">{{ $year_summary_list->material_group_id }}</td>
                                        <td style="text-align: center;">{{ $year_summary_list->gpsc_id }}</td>
                                        <td>{{ $year_summary_list->name." (".$year_summary_list->unit.")" }}</td>
                                        <td style="text-align: center;">
                                            @if($year_summary_list->amount == null)
                                                0
                                            @else
                                                {{ number_format($year_summary_list->amount) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($year_summary_list->price == null)
                                                0.00
                                            @else
                                                {{ number_format($year_summary_list->price, 2) }}
                                            @endif
                                        </td>
                                        <td style="text-align: right;">
                                            @if($year_summary_list->total_price == null)
                                                0.00
                                            @else
                                                {{ number_format($year_summary_list->total_price, 2) }}
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($year_summary_list->amount == null)
                                                <b>หมด</b>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td style="text-align: center;"><b>รวม</b></td>
                                    <td style="text-align: center;"><b>{{ $group_id }}</b></td>
                                    <td style="text-align: center;"></td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <b>{{ number_format($group_total_amount) }}</b>
                                    </td>
                                    <td style="text-align: right;">
                                    </td>
                                    <td style="text-align: right;">
                                        <b>{{ number_format($group_total_price, 2) }}</b>
                                    </td>
                                    <td style="text-align: center;">
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center;"><b>รวมทั้งสิ้น</td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <b>{{ number_format($total_amount_all) }}</b>
                                    </td>
                                    <td style="text-align: right;">
                                    </td>
                                    <td style="text-align: right;">
                                        <b>{{ number_format($total_price_all, 2) }}</b>
                                    </td>
                                    <td style="text-align: center;">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            
        });
    </script>
@endsection
@extends('layouts.app')

@section('css')

    <style type="text/css">
        .label_right {
            text-align: right;
        }

        .label_left {
            text-align: left;
        }

        .label_center {
            text-align: center;
        }
    </style>

@endsection

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">รายงาน : สรุปยอดวัสดุคงเหลือ</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20"></h4>
                        <form action="/report/import/search_data" class="form-search" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger error-form" role="alert">กรุณาระบุเงื่อนไขให้ถูกต้องครบถ้วน</div>
                                    <div class="alert alert-danger error-date" role="alert">วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด</div>
                                </div>
                                <div class="col-md-2 label_right">
                                    <label>รูปแบบการค้นหาข้อมูล : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select id="type_search" name="type_search" class="form-control">
                                            <option value="1">แสดงข้อมูลทั้งหมด</option>
                                            <option value="2">ค้นหาตามปีบัญชี</option>
                                            <option value="3">ค้นหาตามช่วงวันที่</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 label_left">
                                    <button type="submit" class="btn btn-danger validate-search">ค้นหา</button>
                                </div>
                                <div class="col-md-4">
                                    
                                </div>
                            </div>
                            <div class="row fiscal_year">
                                <div class="col-md-2 label_right">
                                    <label>ปีบัญชี : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select id="fiscal_year" name="fiscal_year" class="form-control">
                                            @foreach($fiscal_years as $fiscal_year)
                                            <option value="{{ $fiscal_year->fiscal_year }}">{{ $fiscal_year->fiscal_year }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 label_left">
                                    
                                </div>
                                <div class="col-md-4">
                                    
                                </div>
                            </div>
                            <div class="row date">
                                <div class="col-md-2 label_right">
                                    <label>ตั้งแต่วันที่ : </label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control start-date" name="date_from" id="date_from" placeholder="วันที่เริ่มต้น">
                                    </div>
                                </div>
                                <div class="col-md-1 label_center">
                                    <label>ถึง</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control start-date" name="date_to" id="date_to" placeholder="วันที่สิ้นสุด">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fiscal_year").hide();
            $(".date").hide();

            $(".error-form").hide();
            $(".error-date").hide();
        });

        $(document).on("change", "#type_search", function() {
            if($(this).val() == 1){
                $(".fiscal_year").hide();
                $(".date").hide();
            }else if($(this).val() == 2){
                $(".fiscal_year").show();
                $(".date").hide();
            }else{
                $(".fiscal_year").hide();
                $(".date").show();
            }
        });

        $(document).on("click", ".validate-search", function() {
            var type_search = $("#type_search").val();
            var fiscal_year = $("#fiscal_year").val();
            var date_from = $("#date_from").val();
            var date_to = $("#date_to").val();
            if(type_search == 3){
                if(date_from != '' && date_to != ''){
                    if(date_from > date_to){
                        $(".error-form").hide();
                        $(".error-date").show();
                    }else{
                        $(".form-search").submit();
                    }
                }else{
                    $(".error-form").show();
                    $(".error-date").hide();
                }
            }else{
                $(".form-search").submit();
            }
            return false;
        });
    </script>
@endsection
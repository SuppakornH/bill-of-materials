@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">เบิกวัสดุ : รายละเอียดการเบิก</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">รายละเอียดการเบิกวัสดุเอกสารเลขที่ : {!! $Material_exports[0]->document_id !!}</h4>
                        <table width="100%" style="margin-bottom: 20px">
                            <tr>
                                <td style="text-align: left; width: 8%;">วันที่เบิก : </td>
                                <td style="text-align: left; width: 13%;">{!! $Material_exports[0]->thdate !!}</td>
                                <td style="text-align: left; width: 6%;">ปีบัญชี : </td>
                                <td colspan="3" style="text-align: left;">{!! $Material_exports[0]->fiscal_year !!}</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 8%;">จ่ายให้ : </td>
                                <td style="text-align: left; width: 20%;">
                                    {!! $Material_exports[0]->export_to !!}
                                </td>
                                <td style="text-align: left; width: 8%;">ตำแหน่ง : </td>
                                <td style="text-align: left; width: 20%;">
                                    {!! $Material_exports[0]->export_role !!}
                                </td>
                                <td style="text-align: left; width: 12%;">ศูนย์/สำนัก/แผนก : </td>
                                <td style="text-align: left; width: 32%;">
                                    {!! $Material_exports[0]->export_department !!}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 8%;">วัตถุประสงค์ : </td>
                                <td colspan="5" style="text-align: left; width: 92%;">
                                    @if($Material_exports[0]->export_desrciption == null)
                                        -
                                    @else
                                        {!! $Material_exports[0]->export_desrciption !!}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 8%;">หมายเหตุ : </td>
                                <td colspan="5" style="text-align: left; width: 92%;">
                                    @if($Material_exports[0]->remark == null)
                                        -
                                    @else
                                        {!! $Material_exports[0]->remark !!}
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 2%;">ลำดับ</th>
                                    <th style="text-align: center; width: 15%;">รหัสกลุ่มวัสดุ</th>
                                    <th style="text-align: center; width: 15%;">รหัส GPSC</th>
                                    <th style="text-align: center; width: 37%;">รายการวัสดุ</th>
                                    <th style="text-align: center; width: 10%;">จำนวน</th>
                                    <th style="text-align: center; width: 10%;">ราคาต่อหน่วย</th>
                                    <th style="text-align: center; width: 10%;">ราคารวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i = 1; 
                                    $sum_price = 0;
                                    $sum_amount = 0;
                                ?> 
                                @foreach($material_export_details as $material_export_detail) 
                                <?php 
                                    $total = number_format($material_export_detail->amount*$material_export_detail->price, 2, '.', '');
                                    $sum_amount += intval($material_export_detail->amount);
                                    $sum_price += $total;
                                ?>                         
                                <tr>
                                    <td style="text-align: center;">{{ $i++ }}</td>
                                    <td style="text-align: center;">{{ $material_export_detail->material_group_id }}</td>
                                    <td style="text-align: center;">
                                        @if($material_export_detail->gpsc_id == null)
                                            -
                                        @else
                                            {{ $material_export_detail->gpsc_id }}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{ $material_export_detail->name }}</td>
                                    <td style="text-align: center;">{{ number_format($material_export_detail->amount) }}</td>
                                    <td style="text-align: right;">{{ number_format($material_export_detail->price, 2) }}</td>
                                    <td style="text-align: right;">{{ number_format($total, 2) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align: center;"></th>
                                    <th style="text-align: center;">รวมทั้งสิ้น</th>
                                    <th style="text-align: center;"></th>
                                    <th style="text-align: center;"></th>
                                    <th style="text-align: center;">{!! number_format($sum_amount) !!}</th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;">{{ number_format($sum_price, 2) }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var create_btn = "";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-danger' data-toggle='modal' data-target='#modalCreateGroup'>เพิ่มวัสดุนำเข้า</button>";
            $("#dataTable_length").append(create_btn);
        });
    </script>

@endsection
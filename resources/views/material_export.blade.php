@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">เบิกวัสดุ</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20"></h4>
                        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 2%;">ลำดับ</th>
                                    <th style="text-align: center; width: 8%;">ปีบัญชี</th>
                                    <th style="text-align: center; width: 10%;">เลขที่เอกสาร</th>
                                    <th style="text-align: center; width: 16%;">จ่ายให้</th>
                                    <th style="text-align: center; width: 12%;">ตำแหน่ง</th>
                                    <th style="text-align: center; width: 10%;">จำนวนทั้งหมด</th>
                                    <th style="text-align: center; width: 12%;">ราคารวม</th>
                                    <th style="text-align: center; width: 12%;">วันที่เบิก</th>
                                    <th style="text-align: center; width: 10%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($Material_exports as $Material_export)
                                <tr>
                                    <td style="text-align: center;">{{ $i++ }}</td>
                                    <td style="text-align: center;">{{ $Material_export->fiscal_year }}</td>
                                    <td style="text-align: center;">{{ $Material_export->document_id }}</td>
                                    <td style="text-align: center;">{{ $Material_export->export_to }}</td>
                                    <td style="text-align: center;">{{ $Material_export->export_role }}</td>
                                    <td style="text-align: center;">{{ number_format($Material_export->total_amount) }}</td>
                                    <td style="text-align: center;">{{ number_format($Material_export->total_price, 2) }}</td>
                                    <td style="text-align: center;">{{ $Material_export->thdate }}</td>
                                    <td style="text-align: center;">
                                        <a href="/material_export/{{ $Material_export->id }}/detail">
                                            <i class="c-teal-500 ti-receipt" style="font-size: 20px; cursor: pointer;" title="รายละเอียด"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var create_btn = "";
            create_btn += "&nbsp;&nbsp;<a class='btn btn-danger' href='/material_export/add'>เพิ่มรายการเบิก</a>";
            
            $("#dataTable_length").append(create_btn);
        });
    </script>

@endsection
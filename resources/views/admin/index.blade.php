@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="full-container">
            <div class="email-app">
                <div class="email-side-nav remain-height ov-h">
                    <div class="h-100 layers">
                        <div class="p-20 bgc-grey-100 layer w-100"><a href="#" class="btn btn-danger btn-block insert_dept">เพิ่มแผนก </a></div>
                        <div class="scrollable pos-r bdT layer w-100 fxg-1">
                            <ul class="p-20 nav flex-column">
                                @foreach ($depts as $dept)
                                <li class="nav-item">                                    
                                    <a href="javascript:void(0)" class="c-grey-800 cH-blue-500 department" data-id="{{ $dept->id }}">
                                        <div class="peers ai-c jc-sb">
                                            <div class="peer peer-greed"><i class="mR-10 ti-control-record"></i>
                                            <span>{{ $dept->name }}</span>
                                        </div>
                                    </a>                                    
                                </li>   
                                @endforeach                                                           
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="email-wrapper row remain-height pos-r scrollable bgc-white input_form">
                    <div class="email-content open no-inbox-view">
                        <div class="email-compose">
                            <div class="d-n@md+ p-20"><a class="email-side-toggle c-grey-900 cH-blue-500 td-n" href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            <form class="email-compose-body" action="/admin/insert_dept" method="POST">
                                @csrf
                                <h4 class="c-grey-900 mB-20">เพิ่มแผนก</h4>
                                <div class="send-header">
                                    <div class="alert alert-danger data-complete" role="alert">กรุณากรอกข้อมูลให้ครบถ้วน</div>
                                    <div class="alert alert-danger error-password" role="alert">รหัสผ่านไม่ถูกต้อง</div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="dept_name" id="dept_name" placeholder="ชื่อแผนก/ศูนย์/สังกัด">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="username" id="username" placeholder="ชื่อผู้ใช้งาน">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="รหัสผ่าน">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="re_password" id="re_password" placeholder="กรอกรหัสผ่านอีกครั้ง">
                                    </div>
                                </div>
                                <div id="compose-area"></div>
                                <div class="text-right mrg-top-30">
                                    <button class="btn btn-danger form-submit">บันทึก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="email-wrapper row remain-height pos-r scrollable bgc-white show_data">
                    <div class="email-content open no-inbox-view">
                        <div class="email-compose">
                            <div class="d-n@md+ p-20"><a class="email-side-toggle c-grey-900 cH-blue-500 td-n" href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            <form class="email-compose-body">
                                <h4 class="c-grey-900 mB-20">
                                    <span class="space-department-name">
                                        {{ $depts_name }}
                                    </span>
                                </h4>
                                <div class="send-header">
                                    @if($user_id != 'Empty')
                                    <table>
                                        <tr>
                                            <td><p>ชื่อผู้ใช้งาน</p></td>
                                            <td><p>&nbsp;&nbsp;: <span class="space-username">{{ $username }}</span></p></td>
                                        </tr>
                                        <tr>
                                            <td><p>รหัสผ่าน</p></td>
                                            <td>
                                                <p>&nbsp;&nbsp;: 
                                                    <span class="space-encode-password">••••••••••</span>
                                                    <span class="space-decode-password">{{ $password }}</span> 
                                                    &nbsp;&nbsp;
                                                    <i class="ti-eye show-password" title="แสดงรหัสผ่าน" style="cursor: pointer;"></i>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div id="compose-area"></div>
                                <div class="text-left mrg-top-30">
                                    @if($user_id == 'Empty')   
                                        <button class="btn btn-danger create_user" data-toggle="modal" data-target="#modalCreateUser" data-dept="{{ $dept_id }}">เพิ่มผู้ใช้งาน</button>
                                    @else
                                        <button class="btn btn-danger edit_dept" data-toggle="modal" data-target="#modalEditDept" data-dept="{{ $dept_id }}" data-user="{{ $user_id }}">แก้ไขชื่อแผนก</button>
                                        <button class="btn btn-danger edit_password" data-toggle="modal" data-target="#modalEditPassword" data-dept="{{ $dept_id }}" data-user="{{ $user_id }}">เปลี่ยนรหัสผ่าน</button>
                                        <button class="btn btn-danger del_user" data-dept="{{ $dept_id }}" data-user="{{ $user_id }}">ลบ</button>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- modalEditDept -->
                <div class="modal fade" id="modalEditDept" tabindex="-1" role="dialog" aria-labelledby="modalEditDeptLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalEditDeptLabel">แก้ไขชื่อแผนก</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="dept_name" id="dept_name" placeholder="ชื่อแผนก/ศูนย์/สังกัด">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                <button type="button" class="btn btn-danger">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modalEditPassword -->
                <div class="modal fade" id="modalEditPassword" tabindex="-1" role="dialog" aria-labelledby="modalEditPasswordLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalEditPasswordLabel">เปลี่ยนรหัสผ่าน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="รหัสผ่าน">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="re_password" id="re_password" placeholder="กรอกรหัสผ่านอีกครั้ง">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                <button type="button" class="btn btn-danger">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modalCreateUser -->
                <div class="modal fade" id="modalCreateUser" tabindex="-1" role="dialog" aria-labelledby="modalCreateUserLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalCreateUserLabel">เพิ่มผู้ใช้งาน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="ชื่อผู้ใช้งาน">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="รหัสผ่าน">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="re_password" id="re_password" placeholder="กรอกรหัสผ่านอีกครั้ง">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                <button type="button" class="btn btn-danger">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        var pathname = window.location.pathname;
        if(pathname == "/admin/index"){
            hide(".show_data");
        }else{
            hide(".input_form");
        }
        hide(".data-complete");
        hide(".error-password");
        hide(".space-decode-password")
    });

    $(".del_user").on("click",function() {
        swal({
            title: "ต้องการลบผู้ใช้งานใช่หรือไม่ ?",
            icon: "warning",
            buttons: ["ยกเลิก", "ลบ"],
            dangerMode: true
        })
    });

    $(".insert_dept").on("click",function() {
        show(".input_form");
        hide(".show_data"); 
        clear_input_form();     
    });

    $(".show-password").on("click",function() {
        show(".space-decode-password");
        hide(".space-encode-password"); 
    });

    $(".form-submit").on("click",function() {
        var dept_name = $("#dept_name").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var re_password = $("#re_password").val();
        if(dept_name == '' || username == '' || password == '' || re_password == ''){
            show(".data-complete");
            return false;
        }else if(password != re_password){
            hide(".data-complete");
            show(".error-password");
            return false;
        }else{
            hide(".data-complete");
            hide(".error-password");
            return true;
        }
    });

    $(".department").on("click",function() {
        hide(".input_form");
        show(".show_data"); 
        //console.log($(this).data('id'));
    });

    function clear_input_form(){
        clear("#dept_name");
        clear("#username");
        clear("#password");
        clear("#re_password");
    }

    function show(element){
        $(element).show();
    }

    function hide(element){
        $(element).hide();
    }

    function clear(element){
        $(element).val("");
    }
</script>
@endsection
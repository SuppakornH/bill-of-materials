<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>ระบบบัญชีวัสดุ</title>
    <link rel="icon" type="image/png" href="{{ asset('assets/logo/logo.png') }}"/>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', function load() {
            const loader = document.getElementById('loader');
            setTimeout(function() {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="#">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="{{ asset('assets/logo/logo.png') }}" style="padding-top: 8px; width: 50px;" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ระบบบัญชีวัสดุ</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
                    @if( session('role_id') == 1)
                    <li class="nav-item"><a class="sidebar-link" href="/admin/index"><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">จัดการข้อมูลแต่ละแผนก</span></a></li>
                    <li class="nav-item dropdown"><a class="dropdown-toggle" href="#"><span class="icon-holder"><i class="c-teal-500 ti-agenda"></i> </span><span class="title">รายงาน</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                        <ul class="dropdown-menu">
                            <li><a class="sidebar-link" href="#">สรุปยอดวัสดุคงเหลือ</a></li>                            
                        </ul>
                    </li>
                    
                    @else
                    <li class="nav-item"><a class="sidebar-link" href="/index"><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">หน้าแรก</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="/material_import"><span class="icon-holder"><i class="c-deep-orange-500 ti-ruler-pencil"></i> </span><span class="title">นำเข้าวัสดุ</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="/material_export"><span class="icon-holder"><i class="c-red-500 ti-ruler-pencil"></i> </span><span class="title">เบิกวัสดุ</span></a></li>
                    <li class="nav-item dropdown"><a class="dropdown-toggle" href="#"><span class="icon-holder"><i class="c-teal-500 ti-agenda"></i> </span><span class="title">รายงาน</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                        <ul class="dropdown-menu">
                            <li><a class="sidebar-link" href="/report/summary">สรุปยอดวัสดุคงเหลือ</a></li>
                            <!--<li><a class="sidebar-link" href="/report/import">สรุปยอดรายการนำเข้าวัสดุ</a></li>
                            <li><a class="sidebar-link" href="/report/export">สรุปยอดรายการเบิกวัสดุ</a></li>-->
                        </ul>
                    </li>
                    <li class="nav-item dropdown"><a class="dropdown-toggle" href="#"><span class="icon-holder"><i class="c-brown-500 ti-settings"></i> </span><span class="title">ตั้งค่า</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                        <ul class="dropdown-menu">
                            <li><a class="sidebar-link" href="/setting/material_type">ประเภทวัสดุ</a></li>
                            <li><a class="sidebar-link" href="/setting/material_group">กลุ่มวัสดุ</a></li>
                            <li><a class="sidebar-link" href="/setting/material_list">รายการวัสดุ</a></li>
                            <!--<li><a class="sidebar-link" href="/setting/committee_type">ตำแหน่งกรรมการ</a></li>
                            <li><a class="sidebar-link" href="/setting/committee_list">รายชื่อกรรมการ</a></li>-->
                        </ul>
                    </li>
                    @endif

                    <li class="nav-item"><a class="sidebar-link" href="/logout"><span class="icon-holder"><i class="c-red-500 ti-shift-left"></i> </span><span class="title">ออกจากระบบ</span></a></li>
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                    </ul>
                    <ul class="nav-right">
                        
                    </ul>
                </div>
            </div>
            @yield('content')
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>ศูนย์เทคโนโลยีสารสนเทศและการสื่อสาร กรมวิชาการเกษตร</span>
            </footer>
        </div>        
    </div>
    <script src="{{ asset('assets/login/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/login/vendor/bootstrap/js/popper.js') }}"></script>
    <script src="{{ asset('assets/login/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bundle.js') }}"></script>
    @yield('js')
</body>

</html>
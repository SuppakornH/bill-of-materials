@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">ตั้งค่า : ประเภทวัสดุ</h4>
            @if (session('status'))
                {!! session('status') !!}
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20"></h4>
                        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 10%;">ลำดับ</th>
                                    <th style="text-align: center; width: 75%;">ชื่อประเภทวัสดุ</th>
                                    <th style="text-align: center; width: 15%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                @foreach($Material_types as $Material_type)
                                <tr>
                                    <td style="text-align: center;">{{ ++$i }}</td>
                                    <td style="text-align: center;">{{ $Material_type->name }}</td>
                                    <td style="text-align: center;">
                                        <i class="c-orange-500 ti-pencil-alt" onclick="edit_material_type({{ $Material_type->id }})" style="font-size: 20px; cursor: pointer;" title="แก้ไข"></i>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCreateType" tabindex="-1" role="dialog" aria-labelledby="modalCreateTypeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/insert_material_type" class="form-insert" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCreateTypeLabel">เพิ่มประเภทวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-form" role="alert">กรุณากรอกข้อมูล</div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="material_type_name" id="material_type_name" placeholder="ชื่อประเภทวัสดุ">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-insert">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEditType" tabindex="-1" role="dialog" aria-labelledby="modalEditTypeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/update_material_type" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalEditTypeLabel">แก้ไขประเภทวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" name="edit_material_type_name" id="edit_material_type_name" placeholder="ชื่อประเภทวัสดุ">
                        </div>
                        <input type="hidden" name="edit_id" id="edit_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var create_btn = "";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-danger' data-toggle='modal' data-target='#modalCreateType'>เพิ่มประเภทวัสดุ</button>";

            $("#dataTable_length").append(create_btn);

            $(".error-form").hide();
        });

        $(document).on("click", ".validate-insert", function() {
            var name = $("#material_type_name").val();
            if(name == ''){
                $(".error-form").show();
                return false;
            }else{
                $(".form-insert").submit();
            }
            return false;
        });

        function edit_material_type(id){
            $('#modalEditType').modal('show');
            $.ajax({
                url: '/setting/select_material_type',
                type: 'GET',
                data: {id: id},
                success:function(data) {
                    $("#edit_material_type_name").val(data.name);
                    $("#edit_id").val(data.id);
                }
            });
        }
    </script>
@endsection
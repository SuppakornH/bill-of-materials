@extends('layouts.app')

@section('content')
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">ตั้งค่า : รายการวัสดุ</h4>
            @if (session('status'))
                {!! session('status') !!}
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20"></h4>
                        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 10%;">ลำดับ</th>
                                    <th style="text-align: center; width: 15%;">กลุ่มวัสดุ</th>
                                    <th style="text-align: center; width: 20%;">รหัส GPSC</th>
                                    <th style="text-align: center; width: 30%;">ชื่อรายการ</th>
                                    <th style="text-align: center; width: 10%;">หน่วย</th>
                                    <th style="text-align: center; width: 15%;">จัดการ</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($Material_lists as $Material_list)
                                <tr>
                                    <td style="text-align: center;">{{ $i++ }}</td>
                                    <td style="text-align: center;">{{ $Material_list->material_group_id }}</td>
                                    <td style="text-align: center;">
                                        @if($Material_list->gpsc_id == '')
                                            -
                                        @else
                                            {{ $Material_list->gpsc_id }}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{ $Material_list->name }}</td>
                                    <th style="text-align: center;">{{ $Material_list->unit }}</th>
                                    <td style="text-align: center;">
                                        <i class="c-orange-500 ti-pencil-alt" onclick="edit_material_list({{ $Material_list->id }})" style="font-size: 20px; cursor: pointer;" title="แก้ไข"></i>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCreateList" tabindex="-1" role="dialog" aria-labelledby="modalCreateListLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/insert_material_list" class="form-insert" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCreateListLabel">เพิ่มวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-form" role="alert">กรุณากรอก กลุ่มวัสดุ, ชื่อวัสดุ, หน่วย</div>
                        <div class="alert alert-danger error-group" role="alert">ไม่มีกลุ่มวัสดุนี้</div>
                        <div class="form-group">
                            <input list="material_groups" name="material_group_id" id="material_group_id" class="form-control material_groups" placeholder="เลือกกลุ่มวัสดุ" autocomplete="off">
                            <datalist id="material_groups">
                                @foreach($Material_groups as $Material_group)
                                <option value="{{ $Material_group->material_group_id }}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="gpsc_id" id="gpsc_id" placeholder="รหัส GPSC">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="material_list_name" id="material_list_name" placeholder="ชื่อวัสดุ">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="unit" id="unit" placeholder="หน่วย">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-insert">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEditList" tabindex="-1" role="dialog" aria-labelledby="modalEditListLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/setting/update_material_list" class="form-edit" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalEditListLabel">แก้ไขวัสดุ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-edit-form" role="alert">กรุณากรอก กลุ่มวัสดุ, ชื่อวัสดุ, หน่วย</div>
                        <div class="alert alert-danger error-edit-group" role="alert">ไม่มีกลุ่มวัสดุนี้</div>
                        <div class="form-group">
                            <input list="material_groups" name="edit_material_group_id" id="edit_material_group_id" class="form-control material_groups" placeholder="เลือกกลุ่มวัสดุ" autocomplete="off">
                            <datalist id="material_groups">
                                @foreach($Material_groups as $Material_group)
                                <option value="{{ $Material_group->material_group_id }}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="edit_gpsc_id" id="edit_gpsc_id" placeholder="รหัส GPSC">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="edit_material_list_name" id="edit_material_list_name" placeholder="ชื่อวัสดุ">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="edit_unit" id="edit_unit" placeholder="หน่วย">
                        </div>
                        <input type="hidden" name="edit_id" id="edit_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-edit">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCreateListExcel" tabindex="-1" role="dialog" aria-labelledby="modalCreateListExcelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/excel_upload_list" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCreateListExcelLabel">เพิ่มรายการวัสดุด้วย Excel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger error-excel" role="alert">กรุณาเลือกไฟล์ Excel ที่ต้องการนำข้อมูลเข้า</div>
                        <div class="alert alert-danger error-type" role="alert">นำเข้าได้เฉพาะไฟล์ : .xlsx, .csv</div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01"><i class="ti-upload"></i></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="excel_file" id="excel_file" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกไฟล์ Excel : .xlsx, .csv</label>
                            </div>
                        </div>
                        <div style="margin-top: 15px">
                            <a href="/download_excel_list" style="cursor: pointer;">
                                <img src="{{ asset('assets/icon/excel_icon.png') }}" style="width: 30px;">
                                <label style="cursor: pointer;">โหลดแบบฟอร์มไฟล์ Excel</label>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger validate-upload">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var create_btn = "";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-danger' data-toggle='modal' data-target='#modalCreateList'>เพิ่มรายการวัสดุ</button>";
            create_btn += "&nbsp;&nbsp;<button class='btn btn-success' data-toggle='modal' data-target='#modalCreateListExcel'>เพิ่มรายการวัสดุด้วย Excel</button>";

            $("#dataTable_length").append(create_btn);

            $(".error-form").hide();
            $(".error-excel").hide();
            $(".error-type").hide();
            $(".error-group").hide();
            $(".error-edit-form").hide();
            $(".error-edit-group").hide();
        });

        $(document).on("change", ".custom-file :file", function() {
            var filename = $('input[type=file]').val().split('\\').pop();
            var arr = filename.split('.');
            if(arr[1] == 'csv' || arr[1] == 'xlsx'){
                $(".error-excel").hide();
                $(".error-type").hide();
                $("label.custom-file-label").html(filename);
            }else{
                $(".error-excel").hide();
                $(".error-type").show();
                $('input[type=file]').val('');
                $("label.custom-file-label").html('เลือกไฟล์ Excel : .xlsx, .csv');
            }
        });

        $(document).on("click", ".validate-upload", function() {
            var filename = $('#excel_file').val().split('\\').pop();
            if(filename == ''){
                $(".error-excel").show();
                $(".error-type").hide();
                return false;
            }else{
                return true;
            }
        });

        $(document).on("click", ".validate-insert", function() {
            var material_list_name = $('#material_list_name').val();
            var unit = $('#unit').val();
            var material_group_id = $('#material_group_id').val();
            if(material_list_name == '' || unit == '' || material_group_id == ''){
                $(".error-form").show();
                $(".error-group").hide();
                return false;
            }else{
                $.ajax({
                    url: '/setting/check_duplicate',
                    type: 'GET',
                    data: {id: material_group_id},
                    success:function(data) {
                        if(data == false){
                            $(".form-insert").submit();
                        }else{
                            $(".error-group").show();
                            $(".error-form").hide();
                        }
                    }
                });
            }
            return false;
        });

        $(document).on("click", ".validate-edit", function() {
            var material_list_name = $('#edit_material_list_name').val();
            var unit = $('#edit_unit').val();
            var material_group_id = $('#edit_material_group_id').val();
            if(material_list_name == '' || unit == '' || material_group_id == ''){
                $(".error-edit-form").show();
                $(".error-edit-group").hide();
                return false;
            }else{
                $.ajax({
                    url: '/setting/check_duplicate',
                    type: 'GET',
                    data: {id: material_group_id},
                    success:function(data) {
                        if(data == false){
                            $(".form-edit").submit();
                        }else{
                            $(".error-edit-group").show();
                            $(".error-edit-form").hide();
                        }
                    }
                });
            }
            return false;
        });

        function edit_material_list(id){
            $('#modalEditList').modal('show');
            $.ajax({
                url: '/setting/select_material_list',
                type: 'GET',
                data: {id: id},
                success:function(data) {
                    $("#edit_material_group_id").val(data.material_group_id);
                    $("#edit_gpsc_id").val(data.gpsc_id);
                    $("#edit_material_list_name").val(data.name);
                    $("#edit_unit").val(data.unit);
                    $("#edit_id").val(data.id);
                }
            });
        }

        $(document).on("focus", ".material_groups", function() {
            $(this).val('');
        });
    </script>
@endsection
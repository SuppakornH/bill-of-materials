<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');
Route::get('/login', 'UserController@index');

Route::get('/checklogin', 'UserController@login');
Route::get('/logout', 'UserController@logout');

//-----------DEPT ADMIN-------------

Route::get('/index', function () {
    return view('index');
});

//-----------IMPORT-----------------

Route::get('/material_import', 'MaterialController@material_import');
Route::get('/material_import/{id}/detail', 'MaterialController@material_import_detail');
Route::get('/material_import/add', 'MaterialController@material_import_add_page');
Route::post('/material_import/add/insert', 'MaterialController@material_import_add_insert');

//-----------EXPORT----------------

Route::get('/material_export', 'MaterialController@material_export');
Route::get('/material_export/{id}/detail', 'MaterialController@material_export_detail');
Route::get('/material_export/add', 'MaterialController@material_export_add_page');
Route::post('/material_export/add/insert', 'MaterialController@material_export_add_insert');

Route::get('/check_empty_material', 'MaterialController@check_empty_material');

//-----------REPORT----------------

Route::get('/report/summary', 'ReportController@summary');
Route::post('/report/summary/show_main', 'ReportController@show_summary_main');
Route::get('/report/{id}/{fiscal_year}/summary', 'ReportController@show_summary_detail');

Route::get('/report/import', 'ReportController@import');
Route::post('/report/import/search_data', 'ReportController@show_import_main');

Route::get('/report/export', 'ReportController@export');
Route::post('/report/export/search_data', 'ReportController@show_export_main');

//-----------SETTING----------------

Route::get('/setting/material_type', 'SettingController@material_type');
Route::post('/setting/insert_material_type', 'SettingController@insert_material_type');
Route::get('/setting/select_material_type', 'SettingController@select_material_type');
Route::post('/setting/update_material_type', 'SettingController@update_material_type');


Route::get('/setting/material_group', 'SettingController@material_group');
Route::post('/setting/insert_material_group', 'SettingController@insert_material_group');
Route::get('/setting/select_material_group', 'SettingController@select_material_group');
Route::post('/setting/update_material_group', 'SettingController@update_material_group');

Route::get('/download_excel_group', 'ExcelController@download_excel_group');
Route::post('/excel_upload_group', 'ExcelController@import_group');

Route::get('/setting/material_list', 'SettingController@material_list');
Route::post('/setting/insert_material_list', 'SettingController@insert_material_list');
Route::get('/setting/select_material_list', 'SettingController@select_material_list');
Route::post('/setting/update_material_list', 'SettingController@update_material_list');

Route::get('/download_excel_list', 'ExcelController@download_excel_list');
Route::post('/excel_upload_list', 'ExcelController@import_list');

Route::get('/setting/check_duplicate', 'SettingController@check_duplicate');

// ยังไม่ได้ทำ

Route::get('/setting/committee_type', 'SettingController@committee_type');
Route::get('/setting/committee_list', 'SettingController@committee_list');

//-----------SUPER ADMIN-------------

Route::get('/admin/index', 'SuperAdminController@index');
Route::get('/admin/{dept_id}/index', 'SuperAdminController@show_dept');
Route::post('/admin/insert_dept', 'SuperAdminController@insert_dept');

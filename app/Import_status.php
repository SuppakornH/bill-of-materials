<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import_status extends Model
{
    protected $table = 'import_statuses';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['name'];
}

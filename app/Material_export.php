<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_export extends Model
{
    protected $table = 'material_exports';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'document_id',
    	'fiscal_year',
        'total_amount',
        'total_price',
    	'remark',
    	'date_export',
        'username',
    	'export_name',
    	'export_department',
    	'export_role',
    	'export_desrciption'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee_type extends Model
{
    protected $table = 'committee_types';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'name'
    ];
}

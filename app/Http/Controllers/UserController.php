<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    //---------Mains-------------
    function index(Request $request){
        if($request->session()->get('role_id') == 1){
            return redirect('admin/index');
        }else if($request->session()->get('role_id') == 2){
            return redirect('index');
        }else{
            return view('login.login');
        }        
    }

    //----------Events-----------

    function login(Request $request){       
    	$User = \App\User::where('username',$request->username)->get();
    	if(count($User) == 0){
    		return response()->json([
    			"success" => false,
    			"role_id" => null
    		]);
    	}else{
    		if(base64_encode($request->password) === $User[0]->password){
    			$request->session()->put('username', $User[0]->username);
    			$request->session()->put('role_id', $User[0]->role_id);
    			$request->session()->put('dept_id', $User[0]->dept_id);
    			return response()->json([
	    			"success" => true,
	    			"role_id" => $User[0]->role_id
	    		]);
    		}else{
    			return response()->json([
	    			"success" => false,
	    			"role_id" => null
	    		]);
    		}
    	}
    }

    function logout(Request $request){
    	$request->session()->flush();
    	return redirect('login');
    }
}

<?php

namespace App\Http\Controllers;

use App\Imports\MaterialImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Material_group;
use App\Material_list;
use App\Material_import;
use App\Year_Summary;

class ExcelController extends Controller
{
    function import_group(Request $request){
        ini_set('max_execution_time', 0);
        if($request->hasFile('excel_file')){
        	$collection = Excel::toCollection(new MaterialImport, $request->file('excel_file'));
            
        	$duplicate_data = array();

        	if(count($collection[0]) > 1){
        		for($i = 1; $i < count($collection[0]); $i++){
        			if($this->check_duplicate(trim(strval($collection[0][$i][0]))) == true &&
                    $collection[0][$i][0] != NULL){ //มีข้อมูลอยู่แล้ว
        				DB::table('material_groups')->insert([
							'dept_id' => session('dept_id'),
                            'material_type_id' => $request->excel_material_type_id,
							'material_group_id' => trim(strval($collection[0][$i][0])),
							'created_at' => Carbon::now()->format('Y-m-d H:i:s')
						]);

        			}else{ // ยังไม่มีข้อมูล
                        if($collection[0][$i][0] != NULL){
                            array_push($duplicate_data, $collection[0][$i]);
                        }
        			}
        		}
        	}else{
        		$render = "<div class='alert alert-info' style='text-align: center;'>";
        		$render .= "<h4>ไม่มีข้อมูลในไฟล์ Excel</h4>";
        		$render .= "</div>";

        		return redirect('setting/material_group')->with('status', $render);
        	}

        	if(count($duplicate_data) > 0){
        		$render = "<div class='alert alert-danger' style='text-align: center;'>";
	        	$render .= "<h4>อัพโหลดข้อมูลเสร็จสิ้น. พบข้อมูลซ้ำ ดังนี้</h4>";
	        	$render .= "<table class='table table-striped table-bordered' cellspacing='0' border='1' align='center'>";
	        	$render .= "<thead>";
	        	$render .= "<tr>";
	        	$render .= "<th style='text-align: center; width: 100%;'>กลุ่มวัสดุ</th>";
	        	$render .= "</tr>";
	        	$render .= "</thead>";
	        	$render .= "<tbody>";
	        	for($j = 0; $j < count($duplicate_data); $j++){
	        		$render .= "<tr>";
		        	$render .= "<td style='text-align: center;'>".trim(strval($duplicate_data[$j][0]))."</td>";
		        	$render .= "<tr>";
	        	}
	        	$render .= "</tbody>";
	        	$render .= "</table>";
	        	$render .= "</div>";
        	}else{
        		$render = "<div class='alert alert-success' style='text-align: center;'>";
        		$render .= "<h4>อัพโหลดข้อมูลเสร็จสิ้น</h4>";
        		$render .= "</div>";
        	}

        	return redirect('setting/material_group')->with('status', $render);
        }
    }

    function import_list(Request $request){
        ini_set('max_execution_time', 0);
    	if($request->hasFile('excel_file')){
            $collection = Excel::toCollection(new MaterialImport, $request->file('excel_file'));

            $data_to_import = array();
            $data_error = array();

            if(count($collection[0]) > 1){
                for($i = 1; $i < count($collection[0]); $i++){
                    if($this->check_duplicate(trim(strval($collection[0][$i][0]))) == false &&
                        $collection[0][$i][0] != NULL && 
                        $collection[0][$i][2] != NULL && 
                        $collection[0][$i][3] != NULL){

                        $data = \App\Material_list::where('dept_id', session('dept_id'))
                        ->where('name', trim(strval($collection[0][$i][2])))
                        ->get();

                        if(count($data) > 0){
                            $id = $data[0]->id;
                        }else{
                            $id = DB::table('material_lists')->insertGetId([
                                'dept_id' => session('dept_id'),
                                'material_group_id' => trim(strval($collection[0][$i][0])),
                                'gpsc_id' => trim(strval($collection[0][$i][1])),
                                'name' => trim(strval($collection[0][$i][2])),
                                'unit' => trim(strval($collection[0][$i][3])),
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);
                        }

                        if($collection[0][$i][4] != NULL && $collection[0][$i][5] != NULL){
                            $collection[0][$i][6] = $id;
                            array_push($data_to_import, $collection[0][$i]);
                        }
                    }else{
                        array_push($data_error, $collection[0][$i]);
                    }
                }

                $summary = \App\Year_Summary::where('dept_id', session('dept_id'))
                ->get();

                if(count($summary) == 0){ //ตั้งค่าให้ใช้ function ยกยอดข้อมูลได้แค่ครั้งเดียวเท่านั้น ถ้ามีข้อมูลตั้งต้นอยู่แล้วจะไม่เพิ่มใหม่
                    if(count($data_to_import) > 0){
                        /*$Material_import_excel = \App\Material_import::where('dept_id',session('dept_id'))
                        ->where('document_id', 'like', 'IMPORT_EXCEL-%')
                        ->max('document_id');

                        $count = (int)substr($Material_import_excel,-4)+1;

                        if($count == 1){
                            $sorting_code = '0001';
                        }else{
                            if($count >= 1000){
                                $sorting_code = $count;
                            }else if($count > 99 && $count < 1000){
                                $sorting_code = '0'.$count;
                            }else if($count > 9 && $count < 100){
                                $sorting_code = '00'.$count;
                            }else{
                                $sorting_code = '000'.$count;
                            }
                        }

                        $import_excel_document_id = 'IMPORT_EXCEL-'.$sorting_code;*/
                        $import_excel_document_id = 'IMPORT_EXCEL';

                        if((int)Carbon::now()->format('m') < 10){
                            $fiscal_year = Carbon::now()->format('Y')+541;
                        }else{
                            $fiscal_year = Carbon::now()->format('Y')+542;
                        }

                        $total_amount = 0;
                        $total_price = 0;

                        for($i = 0 ; $i < count($data_to_import) ; $i++){
                            $total_amount += $data_to_import[$i][4];
                            $calculate = $data_to_import[$i][5] * $data_to_import[$i][4];
                            $total_price += $calculate;
                        }

                        $id = DB::table('material_imports')->insertGetId([
                            'dept_id' => session('dept_id'),
                            'document_id' => $import_excel_document_id,
                            'fiscal_year' => $fiscal_year,
                            'import_from' => 'ยอดยกมาปี : '.$fiscal_year,
                            'total_amount' => $total_amount,
                            'total_price' => $total_price,
                            'remark' => NULL,
                            'date_import' => Carbon::now()->format('Y-m-d'),
                            'username' => session('username'),
                            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                        for($i = 0 ; $i < count($data_to_import) ; $i++){
                            DB::table('material_imports_detail')->insert([
                                'dept_id' => session('dept_id'),
                                'document_id' => $import_excel_document_id,
                                'fiscal_year' => $fiscal_year,
                                'import_id' => $id,
                                'material_group_id' => trim(strval($data_to_import[$i][0])),
                                'material_id' => $data_to_import[$i][6],
                                'amount' => $data_to_import[$i][4],
                                'price' => $data_to_import[$i][5],
                                'istatus' => '1',
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);

                            DB::table('years_summary')->insert([
                                'dept_id' => session('dept_id'),
                                'fiscal_year' => $fiscal_year,
                                'material_group_id' => trim(strval($data_to_import[$i][0])),
                                'material_id' => $data_to_import[$i][6],
                                'amount' => $data_to_import[$i][4],
                                'price' => $data_to_import[$i][5],
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);
                        }
                    }
                }else{
                    $render = "<div class='alert alert-danger' style='text-align: center;'>";
                    $render .= "<h4>อัพโหลดข้อมูลเสร็จสิ้น. มีข้อมูลยกยอดมาอยู่แล้ว</h4>";
                    $render .= "</div>";

                    return back()->with('status', $render);
                }

                if(count($data_error) > 0){
                    $render = "<div class='alert alert-danger' style='text-align: center;'>";
                    $render .= "<h4>อัพโหลดข้อมูลเสร็จสิ้น. พบข้อมูลผิดพลาด ดังนี้</h4>";
                    $render .= "<table class='table table-striped table-bordered' cellspacing='0' border='1' align='center'>";
                    $render .= "<thead>";
                    $render .= "<tr>";
                    $render .= "<th style='text-align: center; width: 15%;'>กลุ่มวัสดุ</th>";
                    $render .= "<th style='text-align: center; width: 15%;'>รหัส GPSC</th>";
                    $render .= "<th style='text-align: center; width: 60%;'>ชื่อวัสดุ</th>";
                    $render .= "<th style='text-align: center; width: 10%;'>หน่วย</th>";
                    $render .= "</tr>";
                    $render .= "</thead>";
                    $render .= "<tbody>";
                    for($j = 0; $j < count($data_error); $j++){
                        $render .= "<tr>";
                        $render .= "<td style='text-align: center;'>".trim(strval($data_error[$j][0]))."</td>";
                        $render .= "<td style='text-align: center;'>".trim(strval($data_error[$j][1]))."</td>";
                        $render .= "<td style='text-align: center;'>".trim(strval($data_error[$j][2]))."</td>";
                        $render .= "<td style='text-align: center;'>".trim(strval($data_error[$j][3]))."</td>";
                        $render .= "<tr>";
                    }
                    $render .= "</tbody>";
                    $render .= "</table>";
                    $render .= "</div>";
                }else{
                    $render = "<div class='alert alert-success' style='text-align: center;'>";
                    $render .= "<h4>อัพโหลดข้อมูลเสร็จสิ้น</h4>";
                    $render .= "</div>";
                }

                return back()->with('status', $render);
            }else{
                $render = "<div class='alert alert-info' style='text-align: center;'>";
                $render .= "<h4>ไม่มีข้อมูลในไฟล์ Excel</h4>";
                $render .= "</div>";

                return back()->with('status', $render);
            }
        }
    }

    function download_excel_group(Request $request){
    	$path = storage_path('download/excel/import_material_group.xlsx');

		return response()->download($path);
    }

    function download_excel_list(Request $request){
    	$path = storage_path('download/excel/import_material_list_summary.xlsx');
    	
		return response()->download($path);
    }

    //------------Events------------

    function check_duplicate($id){
    	$data = \App\Material_group::where('dept_id', session('dept_id'))
    	->where('material_group_id', $id)
    	->get();

    	if(count($data) > 0){
    		return false;
    	}else{
    		return true;
    	}
    }

}

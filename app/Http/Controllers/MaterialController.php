<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Material_list;
use App\Material_import;
use App\Material_import_detail;
use App\Material_export;
use App\Material_export_detail;

class MaterialController extends Controller
{
	//---------Import------------

	function material_import(Request $request){
		if($request->session()->get('username') == null){
			return redirect('login');
		}

		$Material_import = \App\Material_import::where('dept_id', session('dept_id'))->orderby('id','desc')->get();

		for($i = 0; $i < count($Material_import); $i++){
			$Material_import[$i]->thdate = $this->convert_thshortdate_format_db_to_show($Material_import[$i]->date_import);
		}

		return view('material_import', [
			'Material_imports' => $Material_import
		]);
	}

	function material_import_detail(Request $request, $id){
		if($request->session()->get('username') == null){
			return redirect('login');
		}

		$material_import = \App\Material_import::where('material_imports.dept_id', session('dept_id'))
		->where('material_imports.id', $id)
		->get();

		$material_import[0]->thdate = $this->convert_thfulldate_format_db_to_show($material_import[0]->date_import);

		$material_import_detail = \App\Material_import_detail::leftJoin('material_imports', 'material_imports.id', '=', 'material_imports_detail.import_id')
		->leftJoin('material_lists', 'material_imports_detail.material_id', '=', 'material_lists.id')
		->where('material_imports.dept_id', session('dept_id'))
		->where('material_lists.dept_id', session('dept_id'))
		->where('material_imports.id', $id)
		->get();

		return view('material_import_detail', [
			'Material_imports' => $material_import,
			'material_import_details' => $material_import_detail
		]);
	}

    function material_import_add_page(Request $request){
    	if($request->session()->get('username') == null){
			return redirect('login');
		}		

    	$date = date('m/d/Y');
    	$month = date('m');
    	$fiscal_year = date('Y')+543;

    	$Material_list = \App\Material_list::where('dept_id', $request->session()->get('dept_id'))
    	->orderby('material_group_id')
    	->get();

    	if(intval($month) < 10){
    		$fiscal_year = $fiscal_year-1; 
    	}
    	
    	return view('material_import_add', [
    		'Material_lists' => $Material_list,
			'date' => $date,
			'fiscal_year' => $fiscal_year
		]);
    }

    function material_import_add_insert(Request $request){
    	$data = $this->calculate_total($request->amount, $request->price);
    	$date_import = $this->convert_date_format_to_db($request->date_import);

    	$id = DB::table('material_imports')->insertGetId([
			'dept_id' => session('dept_id'),
			'document_id' => $request->documnet_number,
			'fiscal_year' => $request->fiscal_year,
			'import_from' => $request->import_from,
			'total_amount' => $data[0],
			'total_price' => $data[1],
			'remark' => $request->remark,
			'date_import' => $date_import,
			'username' => session('username'),
			'created_at' => Carbon::now()->format('Y-m-d H:i:s')
		]);

		for($i = 0 ; $i < count($request->material) ; $i++){
			$str = explode("-",$request->material[$i]);
			$material_id = $str[0];
			$material_group_id = $str[1];
			
			DB::table('material_imports_detail')->insert([
				'dept_id' => session('dept_id'),
				'document_id' => $request->documnet_number,
				'fiscal_year' => $request->fiscal_year,
				'import_id' => $id,
				'material_group_id' => $material_group_id,
				'material_id' => $material_id,
				'amount' => $request->amount[$i],
				'price' => $request->price[$i],
				'istatus' => '1',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s')
			]);
		}

		return redirect('material_import');
    }

    //---------Export------------

    function material_export(Request $request){
    	if($request->session()->get('username') == null){
			return redirect('login');
		}

		$Material_export = \App\Material_export::where('dept_id', session('dept_id'))->orderby('id','desc')->get();

		for($i = 0; $i < count($Material_export); $i++){
			$Material_export[$i]->thdate = $this->convert_thshortdate_format_db_to_show($Material_export[$i]->date_export);
		}

    	return view('material_export', [
			'Material_exports' => $Material_export
		]);
    }

    function material_export_detail(Request $request, $id){
		if($request->session()->get('username') == null){
			return redirect('login');
		}

		$material_export = \App\Material_export::where('material_exports.dept_id', session('dept_id'))
		->where('material_exports.id', $id)
		->get();

		$material_export[0]->thdate = $this->convert_thfulldate_format_db_to_show($material_export[0]->date_export);

		$material_export_detail = \App\Material_export_detail::selectRaw('material_lists.name, material_lists.gpsc_id, material_lists.material_group_id, SUM(material_exports_detail.amount) AS amount, material_exports_detail.price')
		->leftJoin('material_lists', 'material_exports_detail.material_id', '=', 'material_lists.id')
		->where('material_exports_detail.dept_id', session('dept_id'))
		->where('material_lists.dept_id', session('dept_id'))
		->where('material_exports_detail.export_id', $id)
		->groupby('material_exports_detail.material_id')
		->groupby('material_exports_detail.price')
		->get();

		return view('material_export_detail', [
			'Material_exports' => $material_export,
			'material_export_details' => $material_export_detail
		]);
	}

    function material_export_add_page(Request $request){
    	if($request->session()->get('username') == null){
			return redirect('login');
		}		

    	$date = date('m/d/Y');
    	$month = date('m');
    	$fiscal_year = date('Y')+543;
    	
    	$Material_list = DB::table('material_lists')
    	->selectRaw('material_lists.*, (material_import.i_amount - IF(material_export.e_amount IS NULL, 0, material_export.e_amount)) as amount')
    	->join(DB::raw(
    		"(SELECT material_imports_detail.material_id, SUM(material_imports_detail.amount) as i_amount
			FROM material_imports_detail
			GROUP BY material_imports_detail.material_id) AS material_import"), 
		    'material_lists.id', '=', 'material_import.material_id'
		)
		->leftJoin(DB::raw(
    		"(SELECT material_exports_detail.material_id, SUM(material_exports_detail.amount) e_amount
			FROM material_exports_detail
			GROUP BY material_exports_detail.material_id) AS material_export"), 
		    'material_lists.id', '=', 'material_export.material_id'
		)
    	->where('material_lists.dept_id', $request->session()->get('dept_id'))
    	->havingRaw('amount > 0')
    	->orderby('material_lists.material_group_id')
    	->orderby('material_lists.id')
        ->get();

    	if(intval($month) < 10){
    		$fiscal_year = $fiscal_year-1; 
    	}
    	
    	return view('material_export_add', [
    		'Material_lists' => $Material_list,
			'date' => $date,
			'fiscal_year' => $fiscal_year
		]);
    }

    function material_export_add_insert(Request $request){
    	$data = $this->calculate_total($request->export_amount, $request->amount_balance);
    	$date_export = $this->convert_date_format_to_db($request->date_export);

    	$id = DB::table('material_exports')->insertGetId([
			'dept_id' => session('dept_id'),
			'document_id' => $request->document_number,
			'fiscal_year' => $request->fiscal_year,
			'total_amount' => $data[0],
			'export_to' => $request->export_to,
			'export_department' => $request->export_dept,
			'export_role' => $request->export_role,
			'export_desrciption' => $request->export_description,
			'remark' => $request->remark,
			'date_export' => $date_export,
			'username' => session('username'),
			'created_at' => Carbon::now()->format('Y-m-d H:i:s')
		]);

		//dd($request);

		for($i = 0 ; $i < count($request->material) ; $i++){ //นำ input มาทีละตัว
			$str = explode("-",$request->material[$i]);
			$material_id = $str[0];
			$material_group_id = $str[1];

			//ส่วนเบิกที่เหลือจากการคำนวณทั้งหมด
			$over_amount = \App\Material_export_detail::where('dept_id', $request->session()->get('dept_id'))
			->where('material_id', $material_id)
			->where('estatus', '1')
			->get();

			//ส่วนนำเข้าที่เหลือจากการคำนวณทั้งหมด
			$import = \App\Material_import_detail::where('dept_id', $request->session()->get('dept_id'))
			->where('material_id', $material_id)
			->where('istatus', '1')
			->get();

			$sum = 0;
			$over = 0;
			$balance = 0;
			$temp = 0;

			//รวม "เหลือ"
			if(count($over_amount) == 0){
				$over = 0;
			}else{
				for($oa = 0; $oa < count($over_amount); $oa++){
					$over += $over_amount[$oa]->over_amount;
				}
			}

			//เหลือ + เบิก
			$total_export = $over + $request->export_amount[$i];
			//จำนวนเบิก
			$export_amount = $request->export_amount[$i];

			//dd(count($import));

			//loop ส่วนที่นำเข้า
			for($ip = 0; $ip < count($import); $ip++){
				$sum += $import[$ip]->amount; // เอานำเข้ามา + แล้วคำนวณทีละอัน

				if($sum < $total_export){ // ถ้าผลรวมส่วนนำเข้าน้อยกว่า เหลือ+เบิก
					// UPDATE status ส่วนนำเข้าที่นำมา +sum
					DB::table('material_imports_detail')
		            ->where('id', $import[$ip]->id)
		            ->update(array('istatus' => 2));

		            //นำจำนวนเหลือ+เบิก หาส่วนต่างที่นำมาคำนวณระหว่าง เหลือ กับ ส่วนนำเข้า
		            $temp = $total_export - $sum;

		            // Insert เบิก Status = 2 
		            DB::table('material_exports_detail')->insert([
						'dept_id' => session('dept_id'),
						'document_id' => $request->document_number,
						'fiscal_year' => $request->fiscal_year,
						'export_id' => $id,
						'material_group_id' => $material_group_id,
						'material_id' => $material_id,
						'amount' => $export_amount,
						'price' => $import[$ip]->price,
						'material_imports_detail_id' => $import[$ip]->id,
						'over_amount' => 0,
						'estatus' => 2,
						'created_at' => Carbon::now()->format('Y-m-d H:i:s')
					]);

		            // นำส่วนต่างมาลบกับยอดเบิกปัจจุบัน
		            $export_amount -= $temp;
		            //dd("1");
				}else{
					if(count($over_amount) != 0 && $total_export >= $import[0]->amount){ // ถ้ามีส่วนของเบิกที่เหลือ
						// ทำการ UPDATE status ส่วนที่นำมาคิดคำนวณ
						DB::table('material_exports_detail')
			            ->where('material_id', $material_id)
			            ->where('estatus', 1)
			            ->update(array('estatus' => 2));
			            //dd("2");
					}

					if($sum == $total_export){ // ถ้าผลรวมส่วนนำเข้าเท่ากับ เหลือ+เบิก
						// UPDATE status ส่วนนำเข้าที่นำมา +sum
						DB::table('material_imports_detail')
			            ->where('id', $import[$ip]->id)
			            ->update(array('istatus' => 2));
						// Insert เบิก Status = 2
						DB::table('material_exports_detail')->insert([
							'dept_id' => session('dept_id'),
							'document_id' => $request->document_number,
							'fiscal_year' => $request->fiscal_year,
							'export_id' => $id,
							'material_group_id' => $material_group_id,
							'material_id' => $material_id,
							'amount' => $export_amount,
							'price' => $import[$ip]->price,
							'material_imports_detail_id' => $import[$ip]->id,
							'over_amount' => 0,
							'estatus' => 2,
							'created_at' => Carbon::now()->format('Y-m-d H:i:s')
						]);
						//dd("3");
			            // loop break
						break;
					}else{ // ถ้าผลรวมส่วนนำเข้ามากกว่า เหลือ+เบิก
						if($ip == 0){ //ถ้าส่วนของนำเข้าที่นำมาคำนวณพึ่งเป็นตัวแรก
							//ใช้ $request->export_amount ตัวเดิม
							DB::table('material_exports_detail')->insert([
								'dept_id' => session('dept_id'),
								'document_id' => $request->document_number,
								'fiscal_year' => $request->fiscal_year,
								'export_id' => $id,
								'material_group_id' => $material_group_id,
								'material_id' => $material_id,
								'amount' => $export_amount,
								'price' => $import[$ip]->price,
								'material_imports_detail_id' => $import[$ip]->id,
								'over_amount' => $export_amount,
								'estatus' => 1,
								'created_at' => Carbon::now()->format('Y-m-d H:i:s')
							]);
							//dd("4");
						}else{ //ถ้าไม่ใช่
							//นำส่วนเบิกตัวล่าสุดมาลบออกจาก sum
							$sum -= $import[$ip]->amount;
							//คำนวณเบิกคงเหลือ balance
							$balance = ($total_export - $sum) - $export_amount;
							// Insert เบิก Status = 1
							DB::table('material_exports_detail')->insert([
								'dept_id' => session('dept_id'),
								'document_id' => $request->document_number,
								'fiscal_year' => $request->fiscal_year,
								'export_id' => $id,
								'material_group_id' => $material_group_id,
								'material_id' => $material_id,
								'amount' => $export_amount,
								'price' => $import[$ip]->price,
								'material_imports_detail_id' => $import[$ip]->id,
								'over_amount' => $balance,
								'estatus' => 1,
								'created_at' => Carbon::now()->format('Y-m-d H:i:s')
							]);
							//dd("5");
						} 
						// loop break
						break; 
					}
				}
			}
		} // สิ้นสุด

		//ดึงข้อมูลนำเข้าที่พึ่ง INSERT ไปทั้งหมดมาคำนวณ Price total
		$export_detail = \App\Material_export_detail::where('dept_id', $request->session()->get('dept_id'))
		->where('export_id', $id)
		->get();

		$total_price = 0;

		if(count($export_detail) > 0){
			for($ex = 0; $ex < count($export_detail); $ex++){
	    		$sum = $export_detail[$ex]->amount*$export_detail[$ex]->price;
	    		$total_price += $sum;
			}
		}

		DB::table('material_exports')
        ->where('id', $id)
        ->update(array('total_price' => $total_price));

		return redirect('material_export');
    }

    function check_empty_material(Request $request){
    	$Material_list = DB::table('material_lists')
        ->selectRaw('material_lists.*,
        	((
				SELECT SUM(material_imports_detail.amount)
				FROM material_imports_detail
				WHERE  `material_imports_detail`.`dept_id` = '.$request->session()->get('dept_id').' 
   						AND `material_imports_detail`.`material_id` = "'.$request->material_id.'" 
			)
			-
			(
				SELECT IF(SUM(material_exports_detail.amount) IS NULL, 0, SUM(material_exports_detail.amount))
				FROM material_exports_detail
				WHERE  `material_exports_detail`.`dept_id` = '.$request->session()->get('dept_id').' 
   						AND `material_exports_detail`.`material_id` = "'.$request->material_id.'" 
			)) AS amount')
        ->where('material_lists.dept_id', $request->session()->get('dept_id'))
    	->where('material_lists.id', $request->material_id)
        ->get();

        return $Material_list[0]->amount;
    }

    //------------Events------------

    function calculate_total($amount, $price){
    	$sum = 0;
    	$sum_amount = 0;
    	$sum_total = 0;

    	for($i = 0; $i < count($amount); $i++){
    		$sum_amount += $amount[$i];
    		$sum = $amount[$i]*$price[$i];
    		$sum_total += $sum;
    	}

    	$data = array(
    		$sum_amount,
    		$sum_total
    	);

    	return $data;
    }

    function convert_date_format_to_db($date){
    	$array = explode("/",$date);
    	return $array[2].'-'.$array[0].'-'.$array[1];
    }

    function convert_thshortdate_format_db_to_show($date){
    	$array = explode("-",$date);
    	$year = $array[0]+543;
    	$month = $array[1];
    	$day = $array[2];
    	switch ($month) {
		    case "01": $month = "ม.ค."; break;
		    case "02": $month = "ก.พ."; break;
		    case "03": $month = "มี.ค."; break;
		    case "04": $month = "เม.ย."; break;
		    case "05": $month = "พ.ค."; break;
		    case "06": $month = "มิ.ย."; break;
		    case "07": $month = "ก.ค."; break;
		    case "08": $month = "ส.ค."; break;
		    case "09": $month = "ก.ย."; break;
		    case "10": $month = "ต.ค."; break;
		    case "11": $month = "พ.ย."; break;
		    case "12": $month = "ธ.ค."; break;
		}
		return $day." ".$month." ".$year;
    }

    function convert_thfulldate_format_db_to_show($date){
    	$array = explode("-",$date);
    	$year = $array[0]+543;
    	$month = $array[1];
    	$day = $array[2];
    	switch ($month) {
		    case "01": $month = "มกราคม"; break;
		    case "02": $month = "กุมภาพันธ์"; break;
		    case "03": $month = "มีนาคม"; break;
		    case "04": $month = "เมษายน"; break;
		    case "05": $month = "พฤษภาคม"; break;
		    case "06": $month = "มิถุนายน"; break;
		    case "07": $month = "กรกฎาคม"; break;
		    case "08": $month = "สิงหาคม"; break;
		    case "09": $month = "กันยายน"; break;
		    case "10": $month = "ตุลาคม"; break;
		    case "11": $month = "พฤศจิกายน"; break;
		    case "12": $month = "ธันวาคม"; break;
		}
		return $day." ".$month." ".$year;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Department;
use App\User;

class SuperAdminController extends Controller
{
	//---------Mains-------------
	function index(Request $request){
		if($request->session()->get('username') == null){
			return redirect('login');
		}
		$dept = \App\Department::orderBy('name')->get();
		return view('admin.index', [
			'depts' => $dept,
			'dept_id' => 'Empty',
			'depts_name' => 'Empty',
			'user_id' => 'Empty',
			'username' => 'Empty',
			'password' => 'Empty'
		]);
	}

	function show_dept(Request $request, $dept_id){
		if($request->session()->get('username') == null){
			return redirect('login');
		}
		$dept = \App\Department::orderBy('name')->get();
		$dept_name = \App\Department::find($dept_id);
		$user = \App\User::where('dept_id', $dept_id)->get();
		$dept_id = $dept_id;
		$user_id = $user[0]->id;
		$username = $user[0]->username;
		$password = base64_decode($user[0]->password);		
		return view('admin.index', [
			'depts' => $dept,
			'dept_id' => $dept_id,
			'depts_name' => $dept_name->name,
			'user_id' => $user_id,
			'username' => $username,
			'password' => $password
		]);
	}

	//----------Events-----------

	function insert_dept(Request $request){
		$id = DB::table('departments')->insertGetId([
			'name' => $request->dept_name,
			'created_at' => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			'role_id' => '2',
			'dept_id' => $id,
			'username' => $request->username,
			'password' => base64_encode($request->password),
			'created_at' => Carbon::now()->format('Y-m-d H:i:s')
		]);

		return redirect('admin/index');
    }
}

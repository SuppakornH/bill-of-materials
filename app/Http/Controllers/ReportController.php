<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Material_import;
use App\Material_import_detail;
use App\Material_export;
use App\Material_export_detail;
use App\Year_Summary;
use App\Material_type;
use App\Department;

class ReportController extends Controller
{
    function summary(Request $request){
    	if($request->session()->get('username') == null){
			return redirect('login');
		}

        $first = \App\Material_export::select('fiscal_year')
            ->groupBy('fiscal_year');

        $second = \App\Year_Summary::selectRaw('(YEAR(now())+543) AS fiscal_year');

        $fiscal_year = \App\Material_import::select('fiscal_year')
            ->groupBy('fiscal_year')
            ->union($first)
            ->union($second)
            ->orderby('fiscal_year','desc')
            ->get();

        $count = count($fiscal_year);
        unset($fiscal_year[$count-1]);

        return view('reports.summary', [
            'fiscal_years' => $fiscal_year
        ]);
    }

    function show_summary_main(Request $request){
        if($request->session()->get('username') == null){
            return redirect('login');
        }

    	$year_summary = DB::table('material_types')
        ->leftJoin(DB::raw("(
            SELECT 
                material_groups.material_type_id,
                SUM(years_summary.amount) AS years_summary_total_amount,
                SUM(years_summary.amount*years_summary.price) AS years_summary_total_price
            FROM years_summary
            LEFT JOIN material_groups ON years_summary.material_group_id = material_groups.material_group_id
            WHERE years_summary.dept_id = ".$request->session()->get('dept_id')." 
            AND years_summary.fiscal_year = '".($request->fiscal_year-1)."'
            GROUP BY material_groups.material_type_id
            ) AS years_summary"), 
            'years_summary.material_type_id', '=', 'material_types.id'
        )
        ->leftJoin(DB::raw("(
            SELECT 
                material_groups.material_type_id,
                SUM(material_imports_detail.amount) AS material_imports_detail_total_amount,
                SUM(material_imports_detail.amount*material_imports_detail.price) AS material_imports_detail_total_price
            FROM material_imports_detail
            LEFT JOIN material_groups ON material_imports_detail.material_group_id = material_groups.material_group_id
            WHERE material_imports_detail.dept_id = ".$request->session()->get('dept_id')." AND material_imports_detail.fiscal_year = ".$request->fiscal_year."
            GROUP BY material_groups.material_type_id
            ) AS material_imports_detail"),
            'material_imports_detail.material_type_id', '=', 'material_types.id'
        )
        ->leftJoin(DB::raw("(
            SELECT 
                material_groups.material_type_id,
                SUM(material_exports_detail.amount) AS material_exports_detail_total_amount,
                SUM(material_exports_detail.amount*material_exports_detail.price) AS material_exports_detail_total_price
            FROM material_exports_detail
            LEFT JOIN material_groups ON material_exports_detail.material_group_id = material_groups.material_group_id
            WHERE material_exports_detail.dept_id = ".$request->session()->get('dept_id')." AND material_exports_detail.fiscal_year = ".$request->fiscal_year."
            GROUP BY material_groups.material_type_id
            ) AS material_exports_detail"), 
            'material_exports_detail.material_type_id', '=', 'material_types.id'
        )
        ->where('material_types.dept_id', $request->session()->get('dept_id'))
        ->get();

        $department = \App\Department::find($request->session()->get('dept_id'));

        return view('reports.summary_fiscal_year_index', [
            'year_summarys' => $year_summary,
            'department' => $department,
            'fiscal_year' => $request->fiscal_year
        ]);
    }

    function show_summary_detail(Request $request, $id, $fiscal_year){
        if($request->session()->get('username') == null){
            return redirect('login');
        }

        $year_summary_list = DB::table('material_lists')
        ->selectRaw('material_lists.id, material_groups.material_type_id, material_lists.material_group_id, material_lists.gpsc_id, material_lists.name, material_lists.unit, material_detail.amount, material_detail.price, (material_detail.price*material_detail.amount) AS total_price')
        ->leftJoin(DB::raw("(
            SELECT material_imports_detail.material_id, 
            IF(material_exports_detail.amount IS NOT NULL, (material_imports_detail.amount-material_exports_detail.amount), material_imports_detail.amount) AS amount,
            material_imports_detail.price
            FROM material_imports_detail
            LEFT JOIN material_exports_detail ON material_exports_detail.material_imports_detail_id = material_imports_detail.id AND material_exports_detail.estatus = 1
            WHERE material_imports_detail.istatus = 1 
            HAVING amount > 0 ) AS material_detail"), 
            'material_detail.material_id', '=', 'material_lists.id'
        )
        ->leftJoin('material_groups', 'material_groups.material_group_id', '=', 'material_lists.material_group_id')
        ->where('material_lists.dept_id', session('dept_id'))
        ->where('material_groups.material_type_id', $id)
        ->orderby('material_lists.material_group_id')
        ->orderby('material_lists.id')
        ->get();

        $material_type = \App\Material_type::find($id);

        return view('reports.summary_fiscal_year_detail', [
            'year_summary_lists' => $year_summary_list,
            'material_type' => $material_type,
            'fiscal_year' => $fiscal_year
        ]);
    }

    function import(Request $request){
        if($request->session()->get('username') == null){
            return redirect('login');
        }

        $first = \App\Year_Summary::selectRaw('(YEAR(now())+543) AS fiscal_year');

        $fiscal_year = \App\Material_import::select('fiscal_year')
        ->groupBy('fiscal_year')
        ->union($first)
        ->orderby('fiscal_year','desc')
        ->get();

        $count = count($fiscal_year);
        unset($fiscal_year[$count-1]);

        return view('reports.import', [
            'fiscal_years' => $fiscal_year
        ]);
    }

    function show_import_main(Request $request){
        dd($request);
    }

    function export(Request $request){
        if($request->session()->get('username') == null){
            return redirect('login');
        }

        $first = \App\Year_Summary::selectRaw('(YEAR(now())+543) AS fiscal_year');

        $fiscal_year = \App\Material_export::select('fiscal_year')
        ->groupBy('fiscal_year')
        ->union($first)
        ->orderby('fiscal_year','desc')
        ->get();

        return view('reports.export', [
            'fiscal_years' => $fiscal_year
        ]);
    }

    function show_export_main(Request $request){
        dd($request);
    }

    //------------Events------------

    function convert_date_format_to_db($date){
        $array = explode("/",$date);
        return $array[2].'-'.$array[0].'-'.$array[1];
    }

    function convert_thshortdate_format_db_to_show($date){
        $array = explode("-",$date);
        $year = $array[0]+543;
        $month = $array[1];
        $day = $array[2];
        switch ($month) {
            case "01": $month = "ม.ค."; break;
            case "02": $month = "ก.พ."; break;
            case "03": $month = "มี.ค."; break;
            case "04": $month = "เม.ย."; break;
            case "05": $month = "พ.ค."; break;
            case "06": $month = "มิ.ย."; break;
            case "07": $month = "ก.ค."; break;
            case "08": $month = "ส.ค."; break;
            case "09": $month = "ก.ย."; break;
            case "10": $month = "ต.ค."; break;
            case "11": $month = "พ.ย."; break;
            case "12": $month = "ธ.ค."; break;
        }
        return $day." ".$month." ".$year;
    }

    function convert_thfulldate_format_db_to_show($date){
        $array = explode("-",$date);
        $year = $array[0]+543;
        $month = $array[1];
        $day = $array[2];
        switch ($month) {
            case "01": $month = "มกราคม"; break;
            case "02": $month = "กุมภาพันธ์"; break;
            case "03": $month = "มีนาคม"; break;
            case "04": $month = "เมษายน"; break;
            case "05": $month = "พฤษภาคม"; break;
            case "06": $month = "มิถุนายน"; break;
            case "07": $month = "กรกฎาคม"; break;
            case "08": $month = "สิงหาคม"; break;
            case "09": $month = "กันยายน"; break;
            case "10": $month = "ตุลาคม"; break;
            case "11": $month = "พฤศจิกายน"; break;
            case "12": $month = "ธันวาคม"; break;
        }
        return $day." ".$month." ".$year;
    }
}

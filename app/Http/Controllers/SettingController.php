<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Material_type;
use App\Material_group;
use App\Material_list;

class SettingController extends Controller
{
    //--------material_type--------------

    function material_type(Request $request){
        if($request->session()->get('username') == null){
            return redirect('login');
        }

        $Material_type = \App\Material_type::where('dept_id',session('dept_id'))
        ->orderBy('name', 'asc')
        ->get();

        return view('material_type', [
            'Material_types' => $Material_type
        ]);
    }

    function insert_material_type(Request $request){
        DB::table('material_types')->insert([
            'dept_id' => session('dept_id'),
            'name' => $request->material_type_name,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect('setting/material_type');
    }

    function select_material_type(Request $request){
        $data = \App\Material_type::find($request->id);
        return response()->json($data);
    }

    function update_material_type(Request $request){
        DB::table('material_types')
        ->where('id', $request->edit_id)
        ->update(array(
            'name' => $request->edit_material_type_name
        ));

        return redirect('setting/material_type');
    }

    //------------material_group------------------

	function material_group(Request $request){
		if($request->session()->get('username') == null){
			return redirect('login');
		}

		$Material_group = \App\Material_group::selectRaw('material_groups.*, material_types.name as type_name')
        ->leftJoin('material_types', 'material_types.id', '=', 'material_groups.material_type_id')
        ->where('material_groups.dept_id',session('dept_id'))
        ->orderBy('material_type_id', 'asc')
		->orderBy('material_group_id', 'asc')
		->get();

        $Material_type = \App\Material_type::where('dept_id',session('dept_id'))
        ->get();

		return view('material_group', [
			'Material_groups' => $Material_group,
            'Material_types' => $Material_type
		]);
	}

    function insert_material_group(Request $request){
        DB::table('material_groups')->insert([
            'dept_id' => session('dept_id'),
            'material_type_id' => $request->material_type_id,
            'material_group_id' => $request->material_group_id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect('setting/material_group');
    }

    function select_material_group(Request $request){
        $data = \App\Material_group::find($request->id);
        return response()->json($data);
    }

    function update_material_group(Request $request){
        DB::table('material_groups')
        ->where('id', $request->edit_id)
        ->update(array(
            'material_type_id' => $request->edit_material_type_id
        ));

        return redirect('setting/material_group');
    }

    //--------------material_list--------------

	function material_list(Request $request){
		if($request->session()->get('username') == null){
			return redirect('login');
		}

        $Material_group = \App\Material_group::where('dept_id',session('dept_id'))
        ->get();

		$Material_list = \App\Material_list::where('dept_id',session('dept_id'))
        ->orderBy('material_group_id', 'asc')
        ->orderBy('gpsc_id', 'asc')
        ->orderBy('name', 'asc')
		->get();

		return view('material_list', [
            'Material_groups' => $Material_group,
			'Material_lists' => $Material_list
		]);
	}

    function insert_material_list(Request $request){
        DB::table('material_lists')->insert([
            'dept_id' => session('dept_id'),
            'material_group_id' => $request->material_group_id,
            'gpsc_id' => $request->gpsc_id,
            'name' => $request->material_list_name,
            'unit' => $request->unit,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect('setting/material_list');
    }

    function select_material_list(Request $request){
        $data = \App\Material_list::find($request->id);
        return response()->json($data);
    }

    function update_material_list(Request $request){
        $data = \App\Material_list::find($request->edit_id);

        DB::table('material_lists')
        ->where('id', $request->edit_id)
        ->update(array(
            'material_group_id' => $request->edit_material_group_id,
            'gpsc_id' => $request->edit_gpsc_id,
            'name' => $request->edit_material_list_name,
            'unit' => $request->edit_unit
        ));

        if($data->material_group_id != $request->edit_material_group_id){
            DB::table('material_exports_detail')
            ->where('material_id', $request->edit_id)
            ->update(array(
                'material_group_id' => $request->edit_material_group_id,
            ));

            DB::table('material_imports_detail')
            ->where('material_id', $request->edit_id)
            ->update(array(
                'material_group_id' => $request->edit_material_group_id,
            ));
        }

        return redirect('setting/material_list');
    }

	//----------Events-----------

    function check_duplicate(Request $request){
    	$data = \App\Material_group::where('dept_id', session('dept_id'))
    	->where('material_group_id', $request->id)
    	->get();

    	if(count($data) > 0){
    		return response()->json(false);
    	}else{
    		return response()->json(true);
    	}
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year_Summary extends Model
{
    protected $table = 'years_summary';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'fiscal_year',
    	'material_group_id',
    	'material_id',
    	'amount',
    	'price',
    ];
}

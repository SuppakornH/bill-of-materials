<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee_lists extends Model
{
    protected $table = 'committee_lists';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'committee_type_id',
    	'name'
    ];
}

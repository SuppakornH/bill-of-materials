<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_import extends Model
{
    protected $table = 'material_imports';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'document_id',
    	'fiscal_year',
        'import_from',
        'total_amount',
        'total_price',
    	'remark',
    	'date_import',
        'username'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_list extends Model
{
    protected $table = 'material_lists';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'dept_id',
    	'material_group_id',
    	'gpsc_id',
    	'name',
    	'unit'
    ];
}

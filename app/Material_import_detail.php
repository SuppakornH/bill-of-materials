<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_import_detail extends Model
{
    protected $table = 'material_imports_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'document_id',
    	'fiscal_year',
    	'import_id',
    	'material_group_id',
    	'material_id',
    	'amount',
    	'price',
    	'istatus'
    ];
}

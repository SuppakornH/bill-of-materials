<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_group extends Model
{
    protected $table = 'material_groups';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'material_group_id',
    	'name'
    ];
}

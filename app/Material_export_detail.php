<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_export_detail extends Model
{
    protected $table = 'material_exports_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'document_id',
    	'fiscal_year',
    	'export_id',
    	'material_group_id',
    	'material_id',
    	'amount',
        'price',
        'material_imports_detail_id',
        'over_amount',
    	'estatus'
    ];
}

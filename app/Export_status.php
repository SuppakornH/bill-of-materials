<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export_status extends Model
{
    protected $table = 'export_statuses';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['name'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_type extends Model
{
    protected $table = 'material_types';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
    	'dept_id',
    	'name'
    ];
}

**ระบบบัญชีวัสดุ**

-------------------------------------------------------------------------------

**วิธีการติดตั้ง**

```
1. git clone https://gitlab.com/SuppakornH/bill-of-materials.git
2. แก้ไข config database ในไฟล์ .env
3. composer install
4. php artisan key:generate
5. php artisan migrate
6. php artisan db:seed
```


-------------------------------------------------------------------------------

**ปัญหาที่พบในการติดตั้ง**

```
[RuntimeException]
Could not scan for classes inside "database/factories" which does not appear to be a file nor a folder
```


**วิธีแก้ไข**

ให้ทำการลบ database/factories ในไฟล์ composer.json

```
"autoload": {
    "psr-4": {
        "App\\": "app/"
    },
    "classmap": [
        "database/seeds",
        "database/factories" << ลบออก
    ]
}
```


-------------------------------------------------------------------------------

```
C:\xampp\htdocs\bill-of-materials>composer install
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Your requirements could not be resolved to an installable set of packages.

  Problem 1
    - Installation request for league/flysystem 1.0.55 -> satisfiable by league/flysystem[1.0.55].
    - league/flysystem 1.0.55 requires ext-fileinfo * -> the requested PHP extension fileinfo is missing from your system.
  Problem 2
    - Installation request for phpoffice/phpspreadsheet 1.9.0 -> satisfiable by phpoffice/phpspreadsheet[1.9.0].
    - phpoffice/phpspreadsheet 1.9.0 requires ext-fileinfo * -> the requested PHP extension fileinfo is missing from your system.
  Problem 3
    - league/flysystem 1.0.55 requires ext-fileinfo * -> the requested PHP extension fileinfo is missing from your system.
    - laravel/framework v6.0.0 requires league/flysystem ^1.0.8 -> satisfiable by league/flysystem[1.0.55].
    - Installation request for laravel/framework v6.0.0 -> satisfiable by laravel/framework[v6.0.0].

  To enable extensions, verify that they are enabled in your .ini files:
    - C:\Program Files\PHP\v7.3\php.ini
  You can also run `php --ini` inside terminal to see which files are used by PHP in CLI mode.
```

  
**วิธีแก้ไข**

เปลี่ยนมาใช้คำสั่ง
`composer install --ignore-platform-reqs`
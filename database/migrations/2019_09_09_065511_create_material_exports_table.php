<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_exports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('dept_id')->index();
            $table->string('document_id');
            $table->Integer('fiscal_year');
            $table->Integer('total_amount');
            $table->decimal('total_price', 16, 2)->nullable();
            $table->longText('remark')->nullable();
            $table->date('date_export');
            $table->string('username');
            $table->timestamps();

            //Export Details
            $table->longText('export_to');
            $table->longText('export_department')->nullable();
            $table->longText('export_role')->nullable();
            $table->longText('export_desrciption')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_exports');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialExportsDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_exports_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('dept_id')->index();
            $table->string('document_id');
            $table->Integer('fiscal_year');
            $table->Integer('export_id')->index();
            $table->string('material_group_id')->index();
            $table->string('material_id')->index();
            $table->Integer('amount');
            $table->decimal('price', 16, 2);
            $table->Integer('material_imports_detail_id')->index();
            $table->Integer('over_amount');
            $table->Integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_exports_detail');
    }
}

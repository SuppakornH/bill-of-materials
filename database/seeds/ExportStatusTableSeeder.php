<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ExportStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
        [
            ['name' => 'เหลือเศษ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'ไม่เหลือเศษ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')]
        ];
        
        \DB::table('export_statuses')->insert($data);
    }
}

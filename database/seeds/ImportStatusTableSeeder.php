<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ImportStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
        [
            ['name' => 'ยังไม่หมด', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'หมดแล้ว', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') ]
        ];
        
        \DB::table('import_statuses')->insert($data);
    }
}

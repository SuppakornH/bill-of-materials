<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
        [
            ['name' => 'Super Admin', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'Department Admin', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
        ];
        
        \DB::table('roles')->insert($data);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
        [
        	'username' => 'admin',
        	'password' => base64_encode('123456'),
        	'role_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
        
        \DB::table('users')->insert($data);
    }
}

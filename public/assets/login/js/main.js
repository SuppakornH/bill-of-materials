
(function ($) {
    "use strict";


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.login').on('click',function(){
        var username = $(input[0]).val();
        var password = $(input[1]).val();
        $.ajax({
            method: "GET",
            url: "/checklogin",
            data: { username: username, password: password },
            success:function(data) {
                if(data.success == true){
                    if(data.role_id == 1){
                        window.location.href = "/admin/index";
                    }else if(data.role_id == 2){
                        window.location.href = "/index";
                    }
                }else{
                    showValidate(input[0]);
                    showValidate(input[1]);
                }
            }
        });
    });

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);